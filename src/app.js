const express = require('express')
const helmet = require('helmet')
const xss = require('xss-clean')
const compression = require('compression')
const cors = require('cors')
const i18n = require('i18n')
const passport = require('passport')
const httpStatus = require('http-status')
const config = require('./config/config')
const morgan = require('./config/morgan')
const { jwtStrategy } = require('./config/passport')
const { errorConverter, errorHandler } = require('./middlewares/error')
const AppError = require('./utils/AppError')
const router = require('./routes/router')()
const { authLimiter } = require('./middlewares/rateLimiter')
const validate = require('./middlewares/validate')
const validations = require('./validations')
const pagination = require('./middlewares/pagination')
const authenticate = require('./middlewares/auth')

module.exports.init = db => {
  const app = express()

  // Setup some locales - other locales default to en silently
  // Fetch json files from locals and define it ex. en --> english language
  i18n.configure({
    locales: ['en'],
    directory: `${__dirname}/locales`,
    register: global,
  })

  if (config.env !== 'test') {
    app.use(morgan.successHandler)
    app.use(morgan.errorHandler)
  }

  // Set security HTTP headers
  app.use(helmet())

  // i18n init parses req for language headers, cookies, etc.
  app.use(i18n.init)

  // Parse JSON request body and do not allow more than 10mb
  app.use(
    express.json({
      limit: '10mb',
    })
  )

  // Parse urlencoded request body and do not allow more than 10mb
  app.use(
    express.urlencoded({
      extended: true,
      limit: '10mb',
    })
  )

  // sanitize request data
  app.use(xss())

  // gzip compression
  app.use(compression())

  // enable cors
  app.use(cors())
  app.options('*', cors())

  // JWT authentication
  app.use(passport.initialize({}))
  passport.use('jwt', jwtStrategy)

  // Limit repeated failed requests to authentication endpoints
  if (config.env === 'production') {
    app.use('/api/v1/auth', authLimiter)
    app.use('/api/v1/auth/refresh', authLimiter)
  }

  // Inject database, logger and base URL to the request object
  // so we have it handy and use it everywhere
  app.use((req, res, next) => {
    req.base = `${req.protocol}://${req.get('host')}`
    req.logger = morgan.logger
    req.db = db
    req.validate = validate
    req.validations = validations
    return next()
  })

  // Add pagination params in the request object
  // So we can capture it everywhere in the app
  app.use(pagination())

  // Handle specific path excluding mechanism, for auth, register
  const unless = (middleware, ...paths) => (req, res, next) =>
    paths.some(path => path === req.path) ? next() : middleware(req, res, next)

  // Exclude paths from JWT Authentication
  const authExclude = ['/auth', '/auth/refresh', '/register', '/notes/send']

  // Add prefixes for both API and APP using same routes
  app.use('/api/v1', unless(authenticate(), ...authExclude), router)

  // Send back a 404 error for any unknown api request
  app.use((req, res, next) => {
    next(new AppError(httpStatus.NOT_FOUND, 'Not found'))
  })

  // convert error to AppError, if needed
  app.use(errorConverter)

  // handle error
  app.use(errorHandler)

  return app
}
