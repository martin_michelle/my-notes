const dotenv = require('dotenv')
const path = require('path')
const Joi = require('@hapi/joi')

dotenv.config({ path: path.join(__dirname, '../../.env') })

const envVarsSchema = Joi.object()
  .keys({
    MYSQL_HOST: Joi.string()
      .required()
      .description('MySQL host endpoint'),
    MYSQL_PORT: Joi.string()
      .default(3306)
      .description('MySQL port'),
    MYSQL_USER: Joi.string()
      .required()
      .description('MySQL user'),
    MYSQL_PASS: Joi.string()
      .required()
      .description('MySQL password'),
    MYSQL_DATABASE: Joi.string()
      .default(300)
      .description('MySQL database to connect with'),
    MYSQL_CONNECTION_LIMIT: Joi.number()
      .required()
      .description('MySQL connection limit'),
    NODE_ENV: Joi.string()
      .valid('production', 'development', 'test')
      .required(),
    PORT: Joi.number().default(3000),
    JWT_SECRET: Joi.string()
      .required()
      .description('JWT secret key'),
    JWT_ACCESS_EXPIRATION_MINUTES: Joi.number()
      .default(30)
      .description('minutes after which access tokens expire'),
    JWT_REFRESH_EXPIRATION_DAYS: Joi.number()
      .default(30)
      .description('days after which refresh tokens expire'),
    MAX_ROWS_PER_PAGE: Joi.number()
      .default(20)
      .description('Pagination - Max rows per page'),
    REG_URL: Joi.string(),
  })
  .unknown()

const { value: envVars, error } = envVarsSchema.prefs({ errors: { label: 'key' } }).validate(process.env)

if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}

module.exports = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  jwt: {
    secret: envVars.JWT_SECRET,
    accessExpirationMinutes: envVars.JWT_ACCESS_EXPIRATION_MINUTES,
    refreshExpirationDays: envVars.JWT_REFRESH_EXPIRATION_DAYS,
    resetPasswordExpirationMinutes: 10,
  },
  mysql: {
    host: envVars.MYSQL_HOST,
    port: envVars.MYSQL_PORT,
    user: envVars.MYSQL_USER,
    pass: envVars.MYSQL_PASS,
    db: envVars.MYSQL_DATABASE,
    connectionLimit: envVars.MYSQL_CONNECTION_LIMIT,
  },
  email: {
    smtp: {
      host: envVars.SMTP_HOST,
      port: envVars.SMTP_PORT,
      auth: {
        user: envVars.SMTP_USERNAME,
        pass: envVars.SMTP_PASSWORD,
      },
    },
    from: envVars.EMAIL_FROM,
  },
  settings: {
    maxRowsPerPage: envVars.MAX_ROWS_PER_PAG || 10,
  },
  notification: {
    host: envVars.QUEUE_HOST,
    port: envVars.QUEUE_PORT,
    user: envVars.QUEUE_USER,
    password: envVars.QUEUE_PASS,
    queue: envVars.QUEUE_NAME,
  },
}
