const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt')
const config = require('./config')

const jwtOptions = {
  secretOrKey: config.jwt.secret,
  jwtFromRequest: ExtractJwt.fromExtractors([
    ExtractJwt.fromHeader('notes-auth'),
    ExtractJwt.fromHeader('notes-auth'),
    ExtractJwt.fromHeader('auth'),
    ExtractJwt.fromAuthHeaderAsBearerToken(),
  ]),
}

const jwtVerify = async (payload, done) => {
  try {
    if (payload.user) {
      done(null, payload.user)
    }

    done(null, false)
  } catch (error) {
    done(error, false)
  }
}

const jwtStrategy = new JwtStrategy(jwtOptions, jwtVerify)

module.exports = {
  jwtStrategy,
}
