// Cronjob Should run everyday
const database = require('../services/database')
const notificationService = require('../services/notification.service')
const userService = require('../services/user.service')
const logger = require('../config/logger')

const notifyUsersWithMissingNotes = async () => {
  const users = await userService.getUsersThatHaveNotes(database)

  for (let i = 0; i < users.length; i += 1) {
    // eslint-disable-next-line no-await-in-loop
    const getCountOfNotes = await userService.getCountOfNotes(users[i].user_id, database)

    if (getCountOfNotes.length > 0) {
      let message = 'You got new '
      for (let j = 0; j < getCountOfNotes.length; j += 1) {
        if (j === getCountOfNotes.length - 1) message += `${getCountOfNotes[j].count} ${getCountOfNotes[j].note_type} notes `
        else message += `${getCountOfNotes[j].count} ${getCountOfNotes[j].note_type} notes, `
      }

      const subscriberChannel = `notes_for_user_${users[i].identifier}`
      logger.info(`Start sending notification to channel ${subscriberChannel} => ${message}`)
      // eslint-disable-next-line no-await-in-loop
      await notificationService.publishMessage(subscriberChannel, message)
    }
  }

  process.exit()
}

;(async () => {
  await notifyUsersWithMissingNotes()
})()
