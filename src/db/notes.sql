-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 28, 2022 at 02:06 AM
-- Server version: 5.7.17-log
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notes`
--
CREATE DATABASE IF NOT EXISTS `notes` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `notes`;

-- --------------------------------------------------------

--
-- Stand-in structure for view `all_notes`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `all_notes`;
CREATE TABLE IF NOT EXISTS `all_notes` (
`title` varchar(100)
,`message` varchar(1000)
,`note_identifier` varchar(100)
,`user_id` int(11)
,`note_type` varchar(100)
,`is_note_type_active` smallint(6)
,`note_type_id` varchar(100)
,`is_read` smallint(6)
,`is_deleted` smallint(6)
,`mapping_id` int(11)
,`created_on` datetime
);

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

DROP TABLE IF EXISTS `attachments`;
CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type` varchar(100) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `size` double DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attachments`
--

INSERT INTO `attachments` (`id`, `entity_type`, `entity_id`, `path`, `size`, `created_on`) VALUES
(1, 'users', 1, 'avatar-Xc48K5C.jpg', NULL, '2022-01-27 01:12:59'),
(2, 'users', 2, 'avatar-tyNChDI.jpg', NULL, '2022-01-28 00:15:49');

-- --------------------------------------------------------

--
-- Table structure for table `note_type`
--

DROP TABLE IF EXISTS `note_type`;
CREATE TABLE IF NOT EXISTS `note_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_on` date DEFAULT NULL,
  `identifier` varchar(100) DEFAULT NULL,
  `is_active` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `note_type`
--

INSERT INTO `note_type` (`id`, `name`, `created_on`, `identifier`, `is_active`) VALUES
(1, 'congrates', '2022-01-27', 'ee699207-7efd-11ec-9357-b888e31b9e07', 1),
(2, 'invitaions', '2022-01-27', 'ee69999c-7efd-11ec-9357-b888e31b9e07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(500) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(500) NOT NULL,
  `created_on` datetime DEFAULT NULL,
  `identifier` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `created_on`, `identifier`) VALUES
(1, 'Martin-Test', 'martinmichelleebad@gmail.com', 'e807f1fcf82d132f9bb018ca6738a19f', '2022-01-27 01:12:58', '9c708c7f-7efd-11ec-9357-b888e31b9e07'),
(2, 'Martin-Test-2', 'martinmichelleebad2@gmail.com', 'e807f1fcf82d132f9bb018ca6738a19f', '2022-01-28 00:15:48', 'ae53d0cd-7fbe-11ec-b309-b888e31b9e07');

-- --------------------------------------------------------

--
-- Table structure for table `users_notes`
--

DROP TABLE IF EXISTS `users_notes`;
CREATE TABLE IF NOT EXISTS `users_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `message` varchar(1000) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `created_on` datetime DEFAULT NULL,
  `identifier` varchar(100) NOT NULL,
  `is_deleted` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_notes`
--

INSERT INTO `users_notes` (`id`, `title`, `message`, `type_id`, `created_on`, `identifier`, `is_deleted`) VALUES
(1, 'Test Note', 'Test Note Content', 1, '2022-01-28 01:32:57', '7571571a-7fc9-11ec-b309-b888e31b9e07', 0),
(2, 'Test Note', 'Test Note Content', 2, '2022-01-28 02:05:23', 'fcfd7396-7fcd-11ec-b309-b888e31b9e07', 0),
(3, 'Test Note', 'Test Note Content', 2, '2022-01-28 03:24:25', '07b76c2f-7fd9-11ec-b309-b888e31b9e07', 0),
(4, 'Test Note', 'Test Note Content', 2, '2022-01-28 03:24:29', '09ec25c0-7fd9-11ec-b309-b888e31b9e07', 0),
(5, 'Test Note', 'Test Note Content', 2, '2022-01-28 03:29:50', 'c9237360-7fd9-11ec-b309-b888e31b9e07', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_notes_mapping`
--

DROP TABLE IF EXISTS `users_notes_mapping`;
CREATE TABLE IF NOT EXISTS `users_notes_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `note_id` int(11) NOT NULL,
  `is_read` smallint(6) DEFAULT '0',
  `is_deleted` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_notes_mapping`
--

INSERT INTO `users_notes_mapping` (`id`, `user_id`, `note_id`, `is_read`, `is_deleted`) VALUES
(1, 1, 1, 0, 0),
(2, 2, 1, 0, 0),
(3, 1, 2, 0, 1),
(4, 2, 2, 0, 0),
(5, 1, 3, 0, 0),
(6, 2, 3, 0, 0),
(7, 1, 4, 0, 0),
(8, 2, 4, 0, 0),
(9, 1, 5, 0, 0),
(10, 2, 5, 0, 0);

-- --------------------------------------------------------

--
-- Structure for view `all_notes`
--
DROP TABLE IF EXISTS `all_notes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `all_notes`  AS  select `users_notes`.`title` AS `title`,`users_notes`.`message` AS `message`,`users_notes`.`identifier` AS `note_identifier`,`users_notes_mapping`.`user_id` AS `user_id`,`nt`.`name` AS `note_type`,`nt`.`is_active` AS `is_note_type_active`,`nt`.`identifier` AS `note_type_id`,`users_notes_mapping`.`is_read` AS `is_read`,`users_notes_mapping`.`is_deleted` AS `is_deleted`,`users_notes_mapping`.`id` AS `mapping_id`,`users_notes`.`created_on` AS `created_on` from ((`users_notes` join `users_notes_mapping` on((`users_notes`.`id` = `users_notes_mapping`.`note_id`))) join `note_type` `nt` on((`nt`.`id` = `users_notes`.`type_id`))) ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
