const config = require('../config/config')

module.exports = () => {
  // eslint-disable-next-line no-unused-vars
  return function pagination(req, res, next) {
    const { limit, offset, sortField, sortDirection } = { ...req.query }

    // Add pagination object to all requests object
    // So it wont be undefined or need for other checks
    req.pagination = {}
    req.paginationSql = ''

    // Check if there is a limit, if not,
    // we have to set with the default max per page
    if (limit) {
      if (limit > config.settings.maxRowsPerPage) {
        req.pagination.limit = config.settings.maxRowsPerPage
      } else {
        req.pagination.limit = parseInt(limit, 10)
      }

      // Set Offset
      req.pagination.offset = parseInt(offset, 10) || 0
    } else {
      req.pagination.limit = config.settings.maxRowsPerPage
      req.pagination.offset = parseInt(offset, 10) || 0
    }

    req.paginationSql = `LIMIT ${req.pagination.offset}, ${req.pagination.limit}`

    // Check also if there is a sorting object
    if (sortField) {
      req.pagination.sortField = sortField
      req.pagination.sortDirection = sortDirection || 'ASC'
      req.paginationSql = `ORDER BY ${req.pagination.sortField} ${req.pagination.sortDirection} ${req.paginationSql}`
    }

    return next()
  }
}
