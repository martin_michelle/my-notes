const logger = require('../config/logger')
const notesService = require('../services/notes.service')
const notificationService = require('../services/notification.service')

class Notes {
  constructor(database = null) {
    if (database) {
      this.database = database
    }
  }

  setDd(database) {
    if (!this.database) {
      this.database = database
    }
  }

  setLocalization(req) {
    this.__ = req.__
    this.getLocale = req.getLocale
  }

  setPagination(paginationSql) {
    this.paginationSql = paginationSql
  }

  async getAllUserNote(user, data) {
    const pagination = this.paginationSql ? this.paginationSql : ''

    const [rows, count] = await notesService.notesFilter(user, data, pagination, this.database)

    return {
      rows,
      count: count[0].count || 0,
    }
  }

  async deleteNote(user, data) {
    try {
      const { noteIdentifiers } = data

      const deleted = await notesService.deleteNotes(user, noteIdentifiers, this.database)

      if (!deleted) {
        throw new Error('failedToDeleteNotes')
      }

      return true
    } catch (error) {
      logger.error(error.message)
      throw new Error(error.message)
    }
  }

  async createNote(data) {
    const currentConnection = await this.database.getConnection()
    try {
      await currentConnection.beginTransaction()

      // Step [1] - Get notes data from body
      const { title, message, typeIdentifier, userIdentifiers } = data

      // Step [2] - Check on note type
      const noteType = await notesService.getNoteType(typeIdentifier, currentConnection)

      if (!noteType) {
        throw new Error(this.__('noteTypeIsNotExists'))
      }

      const noteTypeId = noteType.id

      // Step [3] - Check on Users
      const users = await notesService.getUsers(userIdentifiers, currentConnection)

      if (!users) {
        throw new Error(this.__('usersDoesNotExists'))
      }

      // Step [4] - Add Note To Db
      const noteId = await notesService.createNewNote(
        {
          title,
          message,
          type_id: noteTypeId,
        },
        this.database
      )

      if (!noteId) {
        throw new Error(this.__('failedToCreateNote'))
      }

      // Step [5] - Create note mapping to selected Users
      const mapNoteToUser = await notesService.createNoteMappingToUsers(noteId, users, currentConnection)

      if (!mapNoteToUser) {
        throw new Error(this.__('failedToMapNote'))
      }

      // Step [5] - Notify All Selected Users
      const notificationMessage = `You get new ${noteType.name} message.`

      // no need validation there because notification failure is not a big deal
      // To handle large number of users should user redis as queue and send notification one by one in the background
      await notificationService.sendNotificationsToUsers(users, notificationMessage)

      await currentConnection.commit()
      await currentConnection.release()

      return true
    } catch (error) {
      logger.error(error.message)
      await currentConnection.rollback()
      await currentConnection.release()
      throw new Error(error.message)
    }
  }
}

module.exports = db => new Notes(db)
