/* eslint-disable security/detect-possible-timing-attacks */
const md5 = require('js-md5')
const logger = require('../config/logger')
const userService = require('../services/user.service')
const fileService = require('../services/files.service')

class User {
  constructor(database = null) {
    if (database) {
      this.database = database
    }
  }

  setDd(database) {
    if (!this.database) {
      this.database = database
    }
  }

  setLocalization(req) {
    this.__ = req.__
    this.getLocale = req.getLocale
  }

  async createUser(data) {
    try {
      // Step [1] - Get email and password of user
      const { userName, password, email, profilePicturePath } = data

      // Step [2] - Check if mail exists

      const userData = await userService.getUserInfo(email, 'email', this.database)

      if (userData) {
        throw new Error(this.__('emailAddressExistsToAnotherUser'))
      }

      // Get value of hash password
      const hashPassword = md5(password)

      // Step [2] - Create new user
      const newUserId = await userService.createUser(
        {
          username: userName,
          password: hashPassword,
          email,
        },
        this.database
      )

      if (!newUserId) {
        throw new Error(this.__('failedToCreateUser'))
      }

      // Step [3] - Get new user data
      const user = await userService.getUserInfo(newUserId, 'id', this.database)

      if (!user) {
        throw new Error(this.__('failedToCreateUser'))
      }

      // Upload Profile Picture if Passed
      if (profilePicturePath) {
        const fileName = await fileService.downloadFile(profilePicturePath, 'avatar')

        if (fileName) {
          const attachment = await fileService.InsertAttachment(
            {
              entity_type: 'users',
              entity_id: newUserId,
              path: fileName,
            },
            this.database
          )

          if (!attachment) {
            throw new Error(this.__('failedToAddAttachment'))
          }
        }
      }

      return user
    } catch (error) {
      logger.error(error.message)
      throw new Error(error.message)
    }
  }

  async authUser(data) {
    try {
      // Step [1] - Get email and password of user
      const { password, email } = data

      // Get value of hash password
      const hashPassword = md5(password)

      // Step [2] - Get user data
      const userData = await userService.getUserInfo(email, 'email', this.database)

      if (!userData) {
        throw new Error(this.__('userNotExists'))
      }

      // Step [3] - Validate password
      if (userData.password !== hashPassword) {
        throw new Error(this.__('invalidPassword'))
      }

      return userData
    } catch (error) {
      logger.error(error.message)
      throw new Error(error.message)
    }
  }
}

module.exports = db => new User(db)
