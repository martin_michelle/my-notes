const { Router } = require('express')
const asyncExec = require('../../../utils/async')
const User = require('../../../models/user')(null)
const authService = require('../../../services/auth.service')

// Schema validation as a middleware
const validateAuth = asyncExec(async (req, res, next) => req.validate(req.validations.auth)(req, res, next))

const validateRegister = asyncExec(async (req, res, next) => req.validate(req.validations.register)(req, res, next))

// authentication routes
module.exports = Router({ mergeParams: true })
  .post(
    '/register',
    validateRegister,
    asyncExec(async (req, res) => {
      User.setDd(req.db)
      User.setLocalization(req)

      const user = await User.createUser(req.body)

      // Generate Token and send to user
      const token = authService.generateAuthTokens(user.id, {
        id: user.id,
        email: user.email,
      })

      const response = {
        info: true,
        token,
      }

      res.json(response)
    })
  )
  .post(
    '/auth',
    validateAuth,
    asyncExec(async (req, res) => {
      User.setDd(req.db)
      User.setLocalization(req)

      const user = await User.authUser(req.body)

      // Generate Token and send to user
      const token = authService.generateAuthTokens(user.id, {
        id: user.id,
        email: user.email,
      })

      const response = {
        info: true,
        token,
      }

      res.json(response)
    })
  )
