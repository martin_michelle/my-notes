const asyncExec = require('../../utils/async')

module.exports.init = (express, db) => {
  const router = express.Router()

  // Init all routes we have
  // router.use('/company', asyncExec(company.init(router, db)))
  return router
}
