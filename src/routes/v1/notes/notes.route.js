const { Router } = require('express')
const asyncExec = require('../../../utils/async')
const Notes = require('../../../models/notes')(null)

const createNote = asyncExec(async (req, res, next) => req.validate(req.validations.createNote)(req, res, next))

// authentication routes
module.exports = Router({ mergeParams: true })
  .post(
    '/notes/send',
    createNote,
    asyncExec(async (req, res) => {
      Notes.setDd(req.db)
      Notes.setLocalization(req)
      Notes.setPagination(req.paginationSql)

      const data = await Notes.createNote(req.body)

      res.json({ error: false, data })
    })
  )
  .get(
    '/notes',
    asyncExec(async (req, res) => {
      Notes.setDd(req.db)
      Notes.setLocalization(req)
      Notes.setPagination(req.paginationSql)

      const result = await Notes.getAllUserNote(req.user, req.query)

      res.json({ error: false, data: result.rows, total: result.count })
    })
  )
  .delete(
    '/notes',
    asyncExec(async (req, res) => {
      Notes.setDd(req.db)
      Notes.setLocalization(req)

      const data = await Notes.deleteNote(req.user, req.body)

      res.json({ error: false, data })
    })
  )
