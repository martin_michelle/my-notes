const moment = require('moment')
const randomToken = require('rand-token')
const config = require('../config/config')
const tokenService = require('./token.service')

const generateAuthTokens = (userId, userObject) => {
  const accessTokenExpires = moment().add(config.jwt.accessExpirationMinutes, 'minutes')
  const accessToken = tokenService.generateToken(userId, userObject, accessTokenExpires)

  const refreshTokenExpires = moment().add(config.jwt.refreshExpirationDays, 'days')
  const refreshToken = randomToken.generate(36)

  return {
    access: {
      token: accessToken,
      expires: accessTokenExpires.toDate(),
    },
    refresh: {
      token: refreshToken,
      expires: refreshTokenExpires.toDate(),
    },
  }
}

module.exports = {
  generateAuthTokens,
}
