const mysql = require('mysql2/promise')
const config = require('../config/config')

module.exports = mysql.createPool({
  host: config.mysql.host,
  port: config.mysql.port,
  user: config.mysql.user,
  password: config.mysql.pass,
  connectionLimit: config.mysql.connectionLimit,
  database: config.mysql.db,
  waitForConnections: true,
  // debug: config.env === 'development',
})
