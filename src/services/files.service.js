const download = require('download')
const randomString = require('randomstring')
const logger = require('../config/logger')

const downloadFile = async (path, type) => {
  try {
    const fileName = `${type}-${randomString.generate(7)}.jpg`
    await download(path, 'dist', { filename: fileName })

    return fileName
  } catch (error) {
    logger.error(error.message)
    return false
  }
}

const InsertAttachment = async (data, connection) => {
  try {
    const [attachment] = await connection.query(
      `
    INSERT INTO attachments SET ?, 
    created_on = NOW()
    `,
      [data]
    )

    return attachment.insertId || false
  } catch (error) {
    logger.error(error.message)
    return false
  }
}

module.exports = {
  downloadFile,
  InsertAttachment,
}
