const logger = require('../config/logger')

const createNewNote = async (data, connection) => {
  try {
    const [note] = await connection.query(
      `
    INSERT INTO users_notes SET ?, 
    created_on = NOW(),
    identifier = UUID()
    `,
      [data]
    )

    return note.insertId || false
  } catch (error) {
    logger.error(error.message)
    return false
  }
}

const getNoteType = async (identifier, connection) => {
  try {
    const [type] = await connection.query(`SELECT * FROM note_type WHERE identifier = ? AND is_active = 1`, [identifier])

    return type[0] || false
  } catch (error) {
    logger.error(error.message)
  }
}

const getUsers = async (identifiers, connection) => {
  try {
    let searchStr = ''

    for (let i = 0; i < identifiers.length; i += 1) {
      searchStr += `"${identifiers[i]}",`
    }

    const [users] = await connection.query(`SELECT * FROM users WHERE identifier in ( ${searchStr.slice(0, -1)} ) `)

    return users || false
  } catch (error) {
    logger.error(error.message)
  }
}

const recursiveUsersMapping = async (users, noteId, index, connection) => {
  try {
    if (users.length === index) {
      return true
    }

    await connection.query(`INSERT INTO users_notes_mapping SET ?`, {
      user_id: users[index].id,
      note_id: noteId,
      is_read: 0,
    })

    await recursiveUsersMapping(users, noteId, index + 1, connection)
  } catch (error) {
    logger.error(error.message)
    return false
  }
}

const createNoteMappingToUsers = async (noteId, users, connection) => {
  try {
    await recursiveUsersMapping(users, noteId, 0, connection)
    return true
  } catch (error) {
    logger.error(error.message)
    return false
  }
}

const deleteNotes = async (user, noteIds, connection) => {
  try {
    let searchStr = ''

    for (let i = 0; i < noteIds.length; i += 1) {
      searchStr += `"${noteIds[i]}",`
    }

    const [getMappingIds] = await connection.query(
      `SELECT mapping_id FROM all_notes 
    WHERE is_deleted = 0 AND user_id = ?
    AND note_identifier in ( ${searchStr.slice(0, -1)} )
    `,
      [user.id]
    )

    if (getMappingIds.length > 0) {
      let mapIds = ''
      for (let i = 0; i < getMappingIds.length; i += 1) {
        mapIds += `${getMappingIds[i].mapping_id},`
      }
      await connection.query(
        `UPDATE users_notes_mapping SET is_deleted = 1 WHERE user_id = ? AND id in ( ${mapIds.slice(0, -1)} )`,
        [user.id]
      )
    }

    return true
  } catch (error) {
    logger.error(error.message)
    return false
  }
}

const notesFilter = async (user, data, pagination, connection) => {
  const readQuery = data && data.read ? ` AND is_read = ${data.read} ` : ''
  const typeIdentifierQuery = data && data.typeIdentifier ? ` AND note_type_id = ${data.typeIdentifier} ` : ''

  const baseQuery = `
    SELECT
     * 
    FROM
     all_notes
    WHERE
      user_id = ${user.id} 
    AND is_note_type_active = 1 
    AND is_deleted = 0
    ${readQuery}
    ${typeIdentifierQuery}
      `

  const [[rows], [count]] = await Promise.all([
    connection.query(`${baseQuery} ${pagination}`),
    connection.query(`SELECT count(*) AS count FROM (${baseQuery}) all_notes`),
  ])

  return [rows, count]
}

module.exports = {
  createNoteMappingToUsers,
  createNewNote,
  getNoteType,
  getUsers,
  notesFilter,
  deleteNotes,
}
