const amqp = require('amqplib')
const logger = require('../config/logger')
const config = require('../config/config')

const queueName = config.notification.queue

logger.info(`Connect to Queue ${queueName}`)

const connectionParams = {
  protocol: 'amqp',
  hostname: config.notification.host,
  port: config.notification.port,
  username: config.notification.user,
  password: config.notification.password,
  locale: 'en_US',
  frameMax: 0,
  heartbeat: 0,
  vhost: '/',
}

const publishMessage = async (channel, message) => {
  try {
    const queueConnection = await amqp.connect(connectionParams)

    if (queueConnection) {
      // Open a new channel
      const queueChannel = await queueConnection.createChannel()

      queueChannel.assertQueue(queueName, { durable: true })

      const queueMessage = { channel, message }

      const exchange = channel

      await queueChannel.assertExchange(exchange, 'direct', { durable: true })
      await queueChannel.assertQueue(queueName, { exclusive: false, durable: true, passive: false })
      await queueChannel.bindQueue(queueName, exchange, queueName)
      await queueChannel.sendToQueue(queueName, Buffer.from(JSON.stringify(queueMessage)), { persistent: true })

      logger.info(`message to channel : ${channel}`)
    }
  } catch (error) {
    logger.error(error.message)
  }
}

const sendNotificationsToUsers = async (users, message) => {
  try {
    for (let i = 0; i < users.length; i += 1) {
      const subscriberChannel = `notes_for_user_${users[i].identifier}`
      logger.info(`Start sending notification to channel ${subscriberChannel} => ${message}`)
      // eslint-disable-next-line no-await-in-loop
      await publishMessage(subscriberChannel, message)
    }
  } catch (error) {
    logger.error(error.message)
  }
}

const receiveMessages = async () => {
  try {
    const queueConnection = await amqp.connect(connectionParams)

    if (queueConnection) {
      // Open a new channel
      const queueChannel = await queueConnection.createChannel()

      queueChannel.prefetch(1000)
      queueChannel.assertQueue(config.notification.queue, { durable: true })

      queueChannel.consume(config.notification.queue, message => {
        if (message.content) {
          queueChannel.ack(message)
        }
      })
    }
  } catch (error) {
    logger.error(error.message)
  }
}

module.exports = {
  publishMessage,
  sendNotificationsToUsers,
  receiveMessages,
}
