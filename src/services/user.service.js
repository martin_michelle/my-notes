const logger = require('../config/logger')

const createUser = async (data, connection) => {
  try {
    const [user] = await connection.query(
      `
    INSERT INTO users SET ?, 
    created_on = NOW(),
    identifier = UUID()
    `,
      [data]
    )

    return user.insertId || false
  } catch (error) {
    logger.error(error.message)
    return false
  }
}

const getUserInfo = async (findByValue, findByField = 'id', connection) => {
  try {
    const [user] = await connection.query(`SELECT * FROM users WHERE ${findByField} = ?`, [findByValue])

    return user[0] || false
  } catch (error) {
    logger.error(error.message)
  }
}

const getUsersThatHaveNotes = async connection => {
  const [users] = await connection.query(`SELECT distinct user_id, users.identifier from all_notes
  inner join users on users.id = all_notes.user_id
  where is_read = 0`)

  return users
}

const getCountOfNotes = async (userId, connection) => {
  const [notes] = await connection.query(
    `SELECT count(note_type_id) as count, 
  note_type from all_notes an 
  where user_id = ? and is_read = 0
  group by note_type_id `,
    [userId]
  )

  return notes
}

module.exports = {
  createUser,
  getUserInfo,
  getUsersThatHaveNotes,
  getCountOfNotes,
}
