const Joi = require('@hapi/joi')

const auth = {
  body: Joi.object().keys({
    email: Joi.string().required(),
    password: Joi.string().required(),
  }),
}

const authRefresh = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required(),
  }),
}

const register = {
  body: Joi.object().keys({
    email: Joi.string()
      .email()
      .required(),
    password: Joi.string().required(),
    userName: Joi.string().required(),
    profilePicturePath: Joi.string(),
  }),
}

const resetPassword = {
  query: Joi.object().keys({
    token: Joi.string().required(),
  }),
  body: Joi.object().keys({
    password: Joi.string().required(),
  }),
}

module.exports = {
  auth,
  authRefresh,
  register,
  resetPassword,
}
