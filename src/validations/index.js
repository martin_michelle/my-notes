module.exports = {
  ...require('./auth.validation'),
  ...require('./notes.validation'),
}
