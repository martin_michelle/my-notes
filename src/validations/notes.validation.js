const Joi = require('@hapi/joi')

const createNote = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    message: Joi.string().required(),
    typeIdentifier: Joi.string().required(),
    userIdentifiers: Joi.array().required(),
  }),
}

module.exports = {
  createNote,
}
